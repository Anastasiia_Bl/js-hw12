// Реалізувати функцію підсвічування клавіш. Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.
// Технічні вимоги:
// У файлі index.html лежить розмітка для кнопок.
// Кожна кнопка містить назву клавіші на клавіатурі
// Після натискання вказаних клавіш - та кнопка, на якій написана ця літера, повинна фарбуватися в синій колір. При цьому якщо якась інша літера вже раніше була пофарбована в синій колір - вона стає чорною. Наприклад за натисканням Enter перша кнопка забарвлюється у синій колір. Далі, користувач натискає S, і кнопка S забарвлюється в синій колір, а кнопка Enter знову стає чорною.


function selectContent (event) {
    // let keycode = event.target.dataset.keycode;
    document.querySelectorAll('.btn').forEach(btn => btn.dataset.keycode === event.code ? btn.classList.add('active') : btn.classList.remove('active'))
};

// document.querySelectorAll('.btn').forEach(el => {
    window.addEventListener('keydown', selectContent);
// })

